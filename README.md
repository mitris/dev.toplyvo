## Installation

##### Step 1
Clone git repo in your project folder
```shell
git clone https://mitris@bitbucket.org/mitris/dev.toplyvo.git
```
##### Step 2
Install via `composer` all project requirements
```shell
cd dev.toplyvo && composer install
```
##### Step 3
Fill `.env` file with your DB access
```shell
vim .env
```

```shell
DB_DATABASE=laravel
DB_USERNAME=root
DB_PASSWORD=
```
##### Step 5
Run migration and seed test data
```shell
php artisan migrate --seed
```
