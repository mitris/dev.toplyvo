<?php

namespace App\Http\Controllers\API;

use App\Models\Manufacturer;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Resources\Json\ResourceCollection;

class ManufacturerController extends Controller
{
    public function index()
    {
        $result = Manufacturer::orderBy('name', 'asc')->paginate();

        return new ResourceCollection($result);
    }

    public function store(Request $request)
    {
        $this->validator($request->all())->validate();

        return Manufacturer::create($request->all());
    }

    public function update(Request $request, Manufacturer $manufacturer)
    {
        $this->validator($request->all())->validate();

        $manufacturer->update($request->all());

        return $manufacturer;
    }

    public function destroy(Manufacturer $manufacturer)
    {
        $manufacturer->delete();

        return response()->json(['message' => 'Success'], 200);
    }

    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required',
            'link' => 'nullable|url',
        ]);
    }
}
