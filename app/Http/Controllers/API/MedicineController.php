<?php

namespace App\Http\Controllers\API;

use App\Models\Medicine;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Resources\Json\ResourceCollection;

class MedicineController extends Controller
{
    public function index(Request $request)
    {
        $query = Medicine::orderBy('name', 'asc')
            ->with(['manufacturer', 'substance']);

        $filter = $request->get('filter');

        if (isset($filter['manufacturer_id'])) {
            $query->where('manufacturer_id', $filter['manufacturer_id']);
        }

        if (isset($filter['substance_id'])) {
            $query->where('substance_id', $filter['substance_id']);
        }

        $result = $query->paginate();

        return new ResourceCollection($result);
    }

    public function store(Request $request)
    {
        $this->validator($request->all())->validate();

        return Medicine::create($request->all());
    }

    public function update(Request $request, Medicine $medicine)
    {
        $this->validator($request->all())->validate();

        $medicine->update($request->all());

        return $medicine;
    }

    public function destroy(Medicine $medicine)
    {
        $medicine->delete();

        return response()->json(['message' => 'Success'], 200);
    }

    protected function validator(array $data)
    {
        return Validator::make($data, [
            'manufacturer_id' => 'required|exists:manufacturers,id',
            'substance_id' => 'required|exists:substances,id',
            'name' => 'required',
            'price' => 'required|numeric',
        ]);
    }
}
