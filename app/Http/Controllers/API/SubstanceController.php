<?php

namespace App\Http\Controllers\API;

use App\Models\Substance;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Resources\Json\ResourceCollection;

class SubstanceController extends Controller
{
    public function index()
    {
        $result = Substance::orderBy('name', 'asc')->paginate();

        return new ResourceCollection($result);
    }

    public function store(Request $request)
    {
        $this->validator($request->all())->validate();

        return Substance::create($request->all());
    }

    public function update(Request $request, Substance $manufacturer)
    {
        $this->validator($request->all())->validate();

        $manufacturer->update($request->all());

        return $manufacturer;
    }

    public function destroy(Substance $manufacturer)
    {
        $manufacturer->delete();

        return response()->json(['message' => 'Success'], 200);
    }

    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required',
        ]);
    }
}
