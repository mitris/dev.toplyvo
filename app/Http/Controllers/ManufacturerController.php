<?php

namespace App\Http\Controllers;

use App\Models\Manufacturer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ManufacturerController extends Controller
{
    public function index()
    {
        $result = Manufacturer::orderBy('name', 'asc')->paginate();

        return view('manufacturer.index', ['result' => $result]);
    }

    public function create()
    {
        $model = new Manufacturer();

        return view('manufacturer.form', ['model' => $model]);
    }

    public function store(Request $request)
    {
        $this->validator($request->all())->validate();

        Manufacturer::create($request->all());

        return redirect()->route('manufacturer.index')->with('success', 'Запись создана.');
    }

    public function edit(Manufacturer $manufacturer)
    {
        return view('manufacturer.form', ['model' => $manufacturer]);
    }

    public function update(Request $request, Manufacturer $manufacturer)
    {
        $this->validator($request->all())->validate();

        $manufacturer->update($request->all());

        return redirect()->route('manufacturer.index')->with('success', 'Запись обновлена.');
    }

    public function destroy(Manufacturer $manufacturer)
    {
        $manufacturer->delete();

        return redirect()->route('manufacturer.index')->with('success', 'Запись удалена.');
    }

    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required',
            'link' => 'nullable|url',
        ]);
    }
}
