<?php

namespace App\Http\Controllers;

use App\Models\Medicine;
use App\Models\Substance;
use App\Models\Manufacturer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class MedicineController extends Controller
{
    public function index(Request $request)
    {
        $query = Medicine::orderBy('name', 'asc')
            ->with(['manufacturer', 'substance']);

        $filter = $request->get('filter');

        if (isset($filter['manufacturer_id'])) {
            $query->where('manufacturer_id', $filter['manufacturer_id']);
        }

        if (isset($filter['substance_id'])) {
            $query->where('substance_id', $filter['substance_id']);
        }

        $result = $query->paginate();

        return view('medicine.index', ['result' => $result]);
    }

    public function create()
    {
        $model = new Medicine();

        $manufacturer_list = Manufacturer::orderBy('name', 'asc')->get()->pluck('name', 'id');
        $substance_list = Substance::orderBy('name', 'asc')->get()->pluck('name', 'id');

        return view('medicine.form', ['model' => $model, 'manufacturer_list' => $manufacturer_list, 'substance_list' => $substance_list]);
    }

    public function store(Request $request)
    {
        $this->validator($request->all())->validate();

        Medicine::create($request->all());

        return redirect()->route('medicine.index')->with('success', 'Запись создана.');
    }

    public function edit(Medicine $medicine)
    {
        $manufacturer_list = Manufacturer::orderBy('name', 'asc')->get()->pluck('name', 'id');
        $substance_list = Substance::orderBy('name', 'asc')->get()->pluck('name', 'id');

        return view('medicine.form', ['model' => $medicine, 'manufacturer_list' => $manufacturer_list, 'substance_list' => $substance_list]);
    }

    public function update(Request $request, Medicine $medicine)
    {
        $this->validator($request->all())->validate();

        $medicine->update($request->all());

        return redirect()->route('medicine.index')->with('success', 'Запись обновлена.');
    }

    public function destroy(Medicine $medicine)
    {
        $medicine->delete();

        return redirect()->route('medicine.index')->with('success', 'Запись удалена.');
    }

    protected function validator(array $data)
    {
        return Validator::make($data, [
            'manufacturer_id' => 'required|exists:manufacturers,id',
            'substance_id' => 'required|exists:substances,id',
            'name' => 'required',
            'price' => 'required|numeric',
        ]);
    }
}
