<?php

namespace App\Http\Controllers;

use App\Models\Substance;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class SubstanceController extends Controller
{
    public function index()
    {
        $result = Substance::orderBy('name', 'asc')->paginate();

        return view('substance.index', ['result' => $result]);
    }

    public function create()
    {
        $model = new Substance();

        return view('substance.form', ['model' => $model]);
    }

    public function store(Request $request)
    {
        $this->validator($request->all())->validate();

        Substance::create($request->all());

        return redirect()->route('substance.index')->with('success', 'Запись создана.');
    }

    public function edit(Substance $substance)
    {
        return view('substance.form', ['model' => $substance]);
    }

    public function update(Request $request, Substance $substance)
    {
        $this->validator($request->all())->validate();

        $substance->update($request->all());

        return redirect()->route('substance.index')->with('success', 'Запись обновлена.');
    }

    public function destroy(Substance $substance)
    {
        $substance->delete();

        return redirect()->route('substance.index')->with('success', 'Запись удалена.');
    }

    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required',
        ]);
    }
}
