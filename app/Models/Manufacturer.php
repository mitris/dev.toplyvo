<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Manufacturer extends Model
{
    use HasFactory;

    protected $fillable = [];

    protected $guarded = [];

    public $timestamps = false;

    public function getLinkAttribute($value)
    {
        return Str::startsWith($value, ['http', 'https']) ? $value : 'http://' . $value;
    }
}
