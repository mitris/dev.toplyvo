<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Medicine extends Model
{
    use HasFactory;

    protected $fillable = [];

    protected $guarded = [];

    public function manufacturer()
    {
        return $this->belongsTo('App\Models\Manufacturer')->withDefault();
    }

    public function substance()
    {
        return $this->belongsTo('App\Models\Substance')->withDefault();
    }
}
