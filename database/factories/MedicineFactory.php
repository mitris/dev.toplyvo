<?php

namespace Database\Factories;

use App\Models\Medicine;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class MedicineFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Medicine::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'manufacturer_id' => rand(1, 50),
            'substance_id' => rand(1, 50),
            'name' => $this->faker->name,
            'price' => $this->faker->randomFloat(2, 0, 1000),
        ];
    }
}
