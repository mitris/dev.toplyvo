<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\{User, Manufacturer, Substance, Medicine};

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $user = User::create([
            'name' => 'user',
            'email' => 'user@site.com',
            'password' => bcrypt('user'),
        ]);
        $user->createToken('default');

        User::factory(10)->create();
        Manufacturer::factory(50)->create();
        Substance::factory(50)->create();
        Medicine::factory(150)->create();
    }
}
