$(function () {
    $('[data-action="destroy"]').on('click', function (e) {
        e.preventDefault();

        if (!confirm('Подтвердите своее действие')) {
            return false;
        }

        $('#app-delete-form').attr('action', $(this).attr('href')).submit();
    });
});
