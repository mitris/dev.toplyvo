@extends('layouts.auth')

@section('content')
    <div class="row justify-content-center">
        <div class="col-md-4">
            {{ aire()->open(route('login')) }}
            <div class="card">
                <h5 class="card-header">Login</h5>
                <div class="card-body">
                    {{ aire()->input('email', 'Email Address') }}
                    {{ aire()->password('password', 'Password') }}
                    {{ aire()->checkbox('remember', 'remember me') }}
                    @if (Route::has('password.request'))
                        <div>
                            <a href="{{ route('password.request') }}">Forgot Your Password?</a>
                        </div>
                    @endif
                </div>
                <div class="card-footer">
                    <a href="{{ route('register') }}" class="btn btn-outline-secondary float-right">Register</a>
                    <button type="submit" class="btn btn-primary">Login</button>
                </div>
            </div>
            {{ aire()->close() }}
        </div>
    </div>
@endsection
