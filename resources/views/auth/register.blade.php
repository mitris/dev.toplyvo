@extends('layouts.auth')

@section('content')
    <div class="row justify-content-center">
        <div class="col-md-4">
            {{ aire()->open(route('register')) }}
            <div class="card">
                <h5 class="card-header">Register</h5>
                <div class="card-body">
                    {{ aire()->input('name', 'Name') }}
                    {{ aire()->input('email', 'Email Address') }}
                    {{ aire()->password('password', 'Password') }}
                    {{ aire()->password('password_confirmation', 'Confirm password') }}
                </div>
                <div class="card-footer">
                    <a href="{{ route('login') }}" class="btn btn-outline-secondary float-right">Login</a>
                    <button type="submit" class="btn btn-primary">Register</button>
                </div>
            </div>
            {{ aire()->close() }}
        </div>
    </div>
@endsection
