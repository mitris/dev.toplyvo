@extends('layouts.app')

@section('content')
    {{ aire()->resourceful($model, 'manufacturer') }}
    <div class="card">
        <h5 class="card-header">
            {{ $model->exists ? 'Редактирование: "' . $model->name . '"' : 'Добавление нового производителя' }}

        </h5>
        <div class="card-body">
            {{ aire()->input('name', 'Название') }}
            {{ aire()->input('link', 'Ссылка') }}
        </div>
        <div class="card-footer">
            <button class="btn btn-primary">Сохранить</button>
            <a href="{{ route('manufacturer.index') }}" class="btn btn-outline-secondary">Назад к списку</a>
            @if($model->exists)
                <a href="{{ route('manufacturer.destroy', $model) }}" class="btn btn-outline-danger float-right" data-action="destroy_model">Удалить</a>
            @endif
        </div>
    </div>
    {{ aire()->close() }}
@endsection
