@extends('layouts.app')

@section('content')
    {{ aire()->resourceful($model, 'medicine') }}
    <div class="card">
        <h5 class="card-header">
            {{ $model->exists ? 'Редактирование: "' . $model->name . '"' : 'Добавление нового лекарственного средства' }}

        </h5>
        <div class="card-body">
            {{ aire()->select($manufacturer_list, 'manufacturer_id', 'Производитель')->prependEmptyOption('Выберите из списка') }}
            {{ aire()->select($substance_list, 'substance_id', 'Действующее вещество')->prependEmptyOption('Выберите из списка') }}
            {{ aire()->input('name', 'Название') }}
            {{ aire()->input('price', 'Цена') }}
        </div>
        <div class="card-footer">
            <button class="btn btn-primary">Сохранить</button>
            <a href="{{ route('medicine.index') }}" class="btn btn-outline-secondary">Назад к списку</a>
            @if($model->exists)
                <a href="{{ route('medicine.destroy', $model) }}" class="btn btn-outline-danger float-right" data-action="destroy_model">Удалить</a>
            @endif
        </div>
    </div>
    {{ aire()->close() }}
@endsection
