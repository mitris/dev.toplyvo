@extends('layouts.app')

@section('content')
    <div class="card">
        <h5 class="card-header">
            Лекарственные средства ({{ $result->total() }})
            <a href="{{ route('medicine.create') }}" class="btn btn-sm btn-secondary">Создать</a>
            @if(request()->has('filter'))
                <a href="{{ route('medicine.index') }}" class="btn btn-sm btn-outline-primary">сбросить фильтр</a>
            @endif
        </h5>
        <table class="table border-none text-center table-hover mb-0">
            <thead>
            <tr>
                <th class="align-middle border-right">ID</th>
                <th class="align-middle border-right">Название</th>
                <th class="align-middle border-right">Производитель</th>
                <th class="align-middle border-right">Ингредиент</th>
                <th class="align-middle">Действия</th>
            </tr>
            </thead>
            <tbody>
            @foreach($result as $item)
                <tr>
                    <td class="align-middle border-right">{{ $item->id }}</td>
                    <td class="align-middle border-right">
                        <a href="{{ route('medicine.edit', $item) }}">{{ $item->name }}</a>
                    </td>
                    <td class="align-middle border-right">
                        @if($item->manufacturer->exists)
                            <a href="{{ route('medicine.index', ['filter[manufacturer_id]' => $item->manufacturer_id]) }}">{{ $item->manufacturer->name }}</a>
                        @else
                            N/A
                        @endif
                    </td>
                    <td class="align-middle border-right">
                        @if($item->substance->exists)
                            <a href="{{ route('medicine.index', ['filter[substance_id]' => $item->substance]) }}">{{ $item->substance->name }}</a>
                        @else
                            N/A
                        @endif
                    </td>
                    <td class="align-middle">
                        <a href="{{ route('medicine.edit', $item) }}" class="btn btn-sm btn-outline-secondary">Редакткировать</a>
                        <a href="{{ route('medicine.destroy', $item) }}" class="btn btn-sm btn-outline-secondary" data-action="destroy">Удалить</a>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        @if($result->hasPages())
            <div class="card-footer">
                {!! $result->withQueryString()->links() !!}
            </div>
        @endif
    </div>
@endsection
