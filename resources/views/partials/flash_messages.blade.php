@if (session('success'))
    <div class="alert alert-success mt-3">
        {{ session('success') }}
    </div>
@endif

@if (session('error') || session('danger'))
    <div class="alert alert-danger mt-3">
        <div>{{ session('error') }}</div>
        <div>{{ session('danger') }}</div>
    </div>
@endif

@if (session('warning'))
    <div class="alert alert-warning mt-3">
        {{ session('warning') }}
    </div>
@endif
