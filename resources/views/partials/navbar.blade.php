<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
    <div class="container">
    <a class="navbar-brand" href="#">{{ env('APP_NAME') }}</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('medicine.index') }}">Лекарственные средства</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('manufacturer.index') }}">Производитель</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('substance.index') }}">Действующее вещество</a>
                </li>
            </ul>
            {{ aire()->open(route('logout'))->class('form-inline my-2 my-lg-0') }}
            <button class="btn btn-outline-secondary">Выход</button>
            {{ aire()->close() }}
        </div>
    </div>
</nav>
