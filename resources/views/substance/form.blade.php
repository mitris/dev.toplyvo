@extends('layouts.app')

@section('content')
    {{ aire()->resourceful($model, 'substance') }}
    <div class="card">
        <h5 class="card-header">
            {{ $model->exists ? 'Редактирование: "' . $model->name . '"' : 'Добавление нового действующего вещества' }}

        </h5>
        <div class="card-body">
            {{ aire()->input('name', 'Название') }}
        </div>
        <div class="card-footer">
            <button class="btn btn-primary">Сохранить</button>
            <a href="{{ route('substance.index') }}" class="btn btn-outline-secondary">Назад к списку</a>
            @if($model->exists)
                <a href="{{ route('substance.destroy', $model) }}" class="btn btn-outline-danger float-right" data-action="destroy_model">Удалить</a>
            @endif
        </div>
    </div>
    {{ aire()->close() }}
@endsection
