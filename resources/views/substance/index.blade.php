@extends('layouts.app')

@section('content')
    <div class="card">
        <h5 class="card-header">
            Действующие вещества ({{ $result->total() }})
            <a href="{{ route('substance.create') }}" class="btn btn-sm btn-secondary">Создать</a>
        </h5>
        <table class="table border-none text-center table-hover mb-0">
            <thead>
            <tr>
                <th class="align-middle border-right">ID</th>
                <th class="align-middle border-right">Название</th>
                <th class="align-middle">Действия</th>
            </tr>
            </thead>
            <tbody>
            @foreach($result as $item)
                <tr>
                    <td class="align-middle border-right">{{ $item->id }}</td>
                    <td class="align-middle border-right">
                        <a href="{{ route('substance.edit', $item) }}">{{ $item->name }}</a>
                    </td>
                    <td class="align-middle">
                        <a href="{{ route('substance.edit', $item) }}" class="btn btn-sm btn-outline-secondary">Редакткировать</a>
                        <a href="{{ route('substance.destroy', $item) }}" class="btn btn-sm btn-outline-secondary" data-action="destroy">Удалить</a>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        @if($result->hasPages())
            <div class="card-footer">
                {!! $result->links() !!}
            </div>
        @endif
    </div>
@endsection
