<?php

use Illuminate\Support\Facades\Route;

Route::post('/user/token', 'API\AuthController@token')->name('auth.token');

Route::middleware('auth:sanctum')->namespace('API')->group(function () {
    Route::resource('medicine', 'MedicineController')->except(['create', 'edit']);
    Route::resource('manufacturer', 'ManufacturerController')->except(['create', 'edit']);
    Route::resource('substance', 'SubstanceController')->except(['create', 'edit']);
});
