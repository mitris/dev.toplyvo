<?php

use Illuminate\Support\Facades\Route;

Route::middleware('auth')->group(function() {
    Route::get('/', 'MedicineController@index')->name('index');

    Route::resource('medicine', 'MedicineController')->except('show');
    Route::resource('manufacturer', 'ManufacturerController')->except('show');
    Route::resource('substance', 'SubstanceController')->except('show');
});

Auth::routes();
